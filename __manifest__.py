# -*- coding: utf-8 -*-
# Copyright 2016, 2017 Openworx
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Nome que vai aparecer",
    "summary": "Sumario.",
    "version": "11.0.1.0.2",
    "category": "test",
    "website": "https://google.com",
    "description": """
		Exemplos de campos do Odoo.
    """,
    'images':[
	],
    "author": "igro",
    "license": "LGPL-3",
    "installable": True,
    "depends": ['aqui é para caso tenha inheritance'],
    "data": [
        'views/teste.xml',#aqui chama o arquivo xml da pasta views
    ],
}
